h = inline('exp(-t).*(heaviside(t) - heaviside(t-4))','t');
x = inline('exp(t).*(heaviside(t) - heaviside(t-1)) + exp(2 -t).*(heaviside(t-1) - heaviside(t-2))','t');
t = -5:0.001:5;
plot(t,h(t));
hold on
plot(t,x(t),'r')