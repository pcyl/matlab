lambda = 2.0e-3;
scrnDist = 5.0e-2;
scrnWdth = 2.4e-2;
xs = 0;
ys = 0;
A = 1;

% Segments
N = 500;
xCoord = linspace(0,scrnDist,N);
yCoord = linspace(-scrnWdth/2,+scrnWdth/2,N);

% Detector Points
[xd,yd] = meshgrid(xCoord,yCoord);

% Calculations
r = sqrt((xd-xs).^2 + (yd-ys).^2); % Distance between Source and Detector
E0 = (A * cos((2*pi*r)/lambda))./r; % Electric field value at a point