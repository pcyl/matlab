lambda = 2.0e-3;
scrnDist = 5.0e-2;
scrnWdth = 2.4e-2;
srcSepn = 1.2e-2; % separation of sources in cm
xs = [0,0];
ys = [-srcSepn/2,srcSepn/2];
A = 1;

% Segments
N = 100;
xCoord = linspace(0,scrnDist,N);
yCoord = linspace(-scrnWdth/2,+scrnWdth/2,N);

% Detector Points
[xd,yd] = meshgrid(xCoord,yCoord);

% Calculations
r1 = sqrt((xd-xs(1)).^2 + (yd-ys(1)).^2); % Distance between Source and Detector
r2 = sqrt((xd-xs(2)).^2 + (yd-ys(2)).^2); % Distance between Source and Detector

E0 = (A * cos((2*pi*r1)/lambda))./r1 + (A * cos((2*pi*r2)/lambda))./r2; % Electric field value at a point

subplot(1,2,2),PseudoColor(xd,yd,E0);
subplot(1,2,1),mesh(xd,yd,E0);