% 1 Slit
figure('Name','1 Slit','NumberTitle','off')
numSlits = 1;
[yd,Itot] = HuygensConstruction2;
plot(yd,Itot);
hold on;
Irel = ManySlitCalculation(yd);
plot(yd,Irel*1.352e5,'red');

% 2 Slit
figure('Name','2 Slits','NumberTitle','off')
numSlits = 2;
[yd,Itot] = HuygensConstruction2;
plot(yd,Itot);
hold on;
Irel = ManySlitCalculation(yd);
plot(yd,Irel*5.3757e5,'red');

% 3 Slit
figure('Name','3 Slits','NumberTitle','off')
numSlits = 3;
[yd,Itot] = HuygensConstruction2;
plot(yd,Itot);
hold on;
Irel = ManySlitCalculation(yd);
plot(yd,Irel*12.017e5,'red');

% 4 Slit
figure('Name','4 Slits','NumberTitle','off')
numSlits = 4;
[yd,Itot] = HuygensConstruction2;
plot(yd,Itot);
hold on;
Irel = ManySlitCalculation(yd);
plot(yd,Irel*22.153e5,'red');

% 5 Slit
figure('Name','5 Slits','NumberTitle','off')
numSlits = 5;
[yd,Itot] = HuygensConstruction2;
plot(yd,Itot);
hold on;
Irel = ManySlitCalculation(yd);
plot(yd,Irel*32.764e5,'red');

% 6 Slit - NEED TO CHANGE INTENSITY VALUES
% figure('Name','6 Slits','NumberTitle','off')
% numSlits = 6;
% [yd,Itot] = HuygensConstruction2;
% plot(yd,Itot);
% hold on;
% Irel = ManySlitCalculation(yd);
% plot(yd,Irel*5.3757e5,'red');