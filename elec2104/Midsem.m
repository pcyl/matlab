% DANIEL GARZANITI, 440162575, dgar4779@uni.sydney.edu.au
% ELEC2104 Midterm solutions
% Please go through code and change all your values first, then run the
% program, read questions for amount of dp as I didn't include this feature.
% Have questions? email me.
% This code doesn't guarantee correct solutions, and I don't take any
% responsiblity if you aren't satisfied with your outcome.
% Revision 2, formatting modified by PATRICK LAI
% Revision 3, Question 1 done by KHALID KHAN
% Reviewed by Daniel Garzaniti, 2/10/2015 

%%
%==============
%= Question 1 =
%==============
q = 1.602e-19;
n = 1e18;%cm^-3
%p = 1e18;%cm^-3
nt = 1e18;%cm^-3
L = 8.9e-4;%cm
A = 0.9e-8; %cm^2
un = 92 + 1270*1/(1+(nt/1.3e17)^0.91);
%up = 48+(447/(1+(nt/(6.3e16))^0.76));
sigma = q*(n*un);
P = 1/sigma;
R = P*L/A;
fprintf('Q1 = %f\n',R);

%%
%==============
%= Question 2 =
%==============
fprintf('Q2 is p-type,p=10^18 cm-3\n');

%%
%==============
%= Question 3 =
%==============
e = 8.85418e-14;
Vr =1.4; % put own val
Vt =0.026;
Na =3e16; %put own val
Nd =7e16;  % put own val
ni =9.65e9; %put own val
q = 1.602e-19;
Vbi = Vt*log((Na*Nd)/(ni)^2);
W = sqrt(((2*11.9*e)/q)*(1/Na + 1/Nd)*(Vbi + Vr));
%convert to um
Q3 = (W/100)/1e-6;
fprintf('Q3 = %f\n',Q3);

%%
%==============
%= Question 4 =
%==============
fprintf('Q4 is decreasing the doping concentration of the p-type side of the junction\n');


%%
%==============
%= Question 5 =
%==============
fprintf('Q5 is Electron drift\n');


%%
%==============
%= Question 6 =
%==============
Vo = 3;   %put own val
Vb = 1.2;  %put own val
R =1700; % change to Ohms
Ix = (Vo - Vb)/R;
% convert to mA
Q6 = Ix/1e-3;
fprintf('Q6 = %f\n',Q6); % do 2dp yourself


%%
%==============
%= Question 7 =
%==============
Vdc = 5;
Idc = 0.030; %amps
f = 50;
T = 1/f;
Vr = 0.08;
C = Idc*((T/2)/Vr);
Q7 = C/1e-6;
fprintf('Q7 = %f\n',Q7);

%%
%==============
%= Question 8 =
%==============
Vdc = 5;
Von = 0.710; % in volts
Vrms = (Vdc+2*Von)/sqrt(2);
fprintf('Q8 = %f\n', Vrms);

%%
%==============
%= Question 9 =
%==============
B= 100;
Vbe = 0.6;
R3 = 152;
R4 = 152;
V1 = 12;
Vth = 12*(500)/1000;
Rth = (500*500)/1000;
Ib = (Vth - Vbe)/(Rth + R4*(B+1));
Q9 = 6 - (Ib*Rth);
fprintf('Q9 = %f\n',Q9); %  take 2dp

%%
%===============
%= Question 10 = 
%===============
B = 100;
Vbe = 0.6;
Vth = 12*(500)/1000;
Rth = (500*500)/1000;
Res3 = 152;  %put own val
Res4 = 152;  % put own val
Ibase = (Vth - Vbe)/(Rth + Res4*(B+1));
Ic = (B*Ibase)/1e-3;
fprintf('Q10 = %f\n',Ic); %take 2dp

%%
%===============
%= Question 11 =
%===============
fprintf('Q11 is Forward-active\n');

%%
%===============
%= Question 12 =
%===============
Vth = 6;
V = 12;
R3 = 145; %put own value
Rth = 250;
nom = Vth/Rth + V/R3;
denom = 1/Rth + 1/R3 + 1/R3;
Vx=nom/denom;
fprintf('Q12 = %f\n',Vx);

%%
%===============
%= Question 13 =
%===============
fprintf('Q13 = 0, No current goes through R4\n');

%%
%===============
%= Question 14 =
%===============
fprintf('Q14 is B is lower and gm is lower\n');
