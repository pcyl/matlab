%HW9

%Q1
Av = 0.73;
beta = (30/0.18)*200*(10^-6);
Vth = 0.4;
gm = Av/(500*(1-Av));
Vgs = (gm/beta) + Vth;
Id = (beta/2)*(Vgs - Vth)^2;
Vsb = 500*Id;
Vgb = Vgs + Vsb;
disp(['Question 1: ', num2str(Vgb)]);

%Q2
Id = 0.41e-3;
Rs = 200;
beta = (20/0.18)*200*(10^-6);
Vth = 0.4;
Irs = (11/10)*Id;
Vsb = Irs*Rs;
Vgb = (2*Id/beta)^(1/2) + Vsb + Vth;
R2 = 10*(Vgb - Vsb)/(Id*1000);
disp(['Question 2: ', num2str(R2)]);

%Q3 
disp(['Question 3: ', '6']);

%Q4 
disp(['Question 4: ', '-1/gm2 / (1/gm1 + 1/gm3)']);

%Q5 
disp(['Question 5: ', '7.413']);

%Q6 
disp(['Question 6: ', '34.49']);

%Q7 
disp(['Question 7: ', '0.134']);
