Is = 5e-16;
Ix = 1.5e-3;
Vt = 0.026;
R = 2e3;

Vr = @(V) Ix - V/R - Is*exp(V/(2*Vt));
Vr1 = fzero(Vr,1);
fprintf('Voltage is %g V/n',Vr1);