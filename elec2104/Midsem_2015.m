%% Midsem Exam

% some constants:
global q 
q = 1.602e-19; %C
global B
B = 100;

%% Q1
% Calculate resistance of a bar of silicon based on concentration of
% n-carriers 
L = 8.9e-4; %Length (cm)
A = 0.9e-8; %Area (cm^2)
n = 1e18; %cm-3
nT=1e18;
mu_n = 92 + ((1270)/(1 + (nT/1.3E17)^0.91));
mu_p = 48 + ((447 )/(1 + (nT/6.3E16)^0.76));
sigma = q*(n*mu_n);
rho = 1/sigma;
R = rho*(L/A);

disp('Question 1');
fprintf('The Resistance is %3.2f ohms', R);
fprintf('\n\n');

clear;

%% Q2
disp('Question 2');
disp('p-type, p = 10^18');
fprintf('\n\n');

%% Q3
disp('Question 3');
e = 8.85418e-14;
Na = 3e16; %cm^-3
Nd = 7e16; %cm^-3
Vr = 1.4; %applied reverse voltage (V)
Vt = 0.026; %V
ni = 9.65e9;
e0 = 11.9*e; %dielectric constant
global q;

Vbi = Vt*log((Na*Nd)/(ni)^2);

W = sqrt(((2*e0)/q)*(1/Na + 1/Nd)*(Vbi + Vr)); %total depletion width

Q3 = (W/100)/1e-6;
fprintf('Depletion width is %.3f um', Q3);
fprintf('\n\n');

%% Q4
disp('Question 4');
disp('decreasing the doping concentration of the p-type side of the junction\n');
fprintf('\n\n');

%% Q5
disp('Question 5');
fprintf('Electron drift');
fprintf('\n\n');

%% Q6
disp('Question 6');

Vo = 3; %V
Vb = 1.2; %V
R = 1.7e3; %ohms

ix = (Vo - Vb)/R;
ix = ix*1e3;

fprintf('Max current is %3.2f mA', ix);
fprintf('\n\n');

%% Q7
disp('Question 7');

Idc = 0.030; %A
f = 50; %Hz
T = f^-1; %s
Vr = 0.08; %V

C = Idc/(f*2*Vr);
C = C*10^6;
fprintf('Capacitance is %3.2f uF', C);
fprintf('\n\n');

%% Q8
disp('Question 8');

Vdon = 0.710; %V
Vdc = 5; %V
RMS = (Vdc + 2*Vdon)/sqrt(2);

fprintf('RMS is %3.3f V', RMS);
fprintf('\n\n');

%% Q9
disp('Question 9');

global B;
Vbe = 0.6; %V
R1 = 500; %ohms
R2 = 500; %ohms
R3 = 152; %ohms
R4 = 152; %ohms
V = 12; %V

Vth = V*(R2/(R1+R2));
Rth = (R1*R2)/(R1+R2);

Ib = (Vth - Vbe)/(Rth + R4*(B+1));

Vb = Vth - (Ib*Rth);

fprintf('Vx is %3.3f V', Vb);
fprintf('\n\n');

%% Q10

disp('Question 10');

global B;
Vbe = 0.6; %V
R1 = 500; %ohms
R2 = 500; %ohms
R3 = 152; %ohms
R4 = 152; %ohms
V = 12; %V

Vth = V*(R2/(R1+R2));
Rth = (R1*R2)/(R1+R2);

Ib = (Vth - Vbe)/(Rth + R4*(B+1));
Ic = (B*Ib)/1e-3;

fprintf('Ic is %.2f mA', Ic);
fprintf('\n\n');

%% Q11
disp('Question 11');
fprintf('Q11 is Forward-active');
fprintf('\n\n');

%% Q12
disp('Question 12');

Ra = 500; %ohms
Rb = 151; %ohms
Rth = 250;
V = 12; %V

Vx = ((V*Rth)/Ra) * (Rb/(Rb+Rth));

fprintf('Vx is %.3f V', Vx);
fprintf('\n\n');

%% Q13
disp('Question 13');

fprintf('no current m8, I4 = 0');
fprintf('\n\n');

%% Q14
disp('Question 14');

fprintf('B is lower, and gm is lower\n\n');
fprintf('Love, Riley and Max, dream team 5eva <3');
fprintf('\n\n');