% Adding functions to path, functions downloaded from elearning.
addpath('/Users/patricklai/Documents/MATLAB/elec2104/Version_2p5_Final/v2p5')
addpath('/Users/patricklai/Documents/MATLAB/elec2104/Version_2p5_Final/v2p5/tdmsSubfunctions')

% extracting data from tdms
irled = TDMS_readTDMSFile('irled.tdms');
irled_timeAxis = 0:irled.propValues{4}{3}:(length(irled.data{4})-1)*irled.propValues{4}{3};

%plot
figure;
plot(irled_timeAxis,irled.data{4});
hold on
plot(irled_timeAxis,irled.data{6});
xlim([161 169]);
title('IR Led');
hold off

% extracting data from tdms
redled = TDMS_readTDMSFile('redled.tdms');
redled_timeAxis = 0:redled.propValues{4}{3}:(length(redled.data{4})-1)*redled.propValues{4}{3};

%plot
figure;
plot(redled_timeAxis,redled.data{4});
hold on
plot(redled_timeAxis,redled.data{6});
xlim([20 30]);
title('Red LED');
hold off