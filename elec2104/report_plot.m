load report_data

%plot ir
figure;
plot(irled_timeAxis,irled.data{4});
hold on
plot(irled_timeAxis,irled.data{6});
xlim([161 169]);
title('IR Led'); xlabel('Time (s)'); ylabel('Amplitude (V)');
hold off

%plot red
figure;
plot(redled_timeAxis,redled.data{4});
hold on
plot(redled_timeAxis,redled.data{6});
xlim([21 29]);
title('Red LED'); xlabel('Time (s)'); ylabel('Amplitude (V)');
hold off