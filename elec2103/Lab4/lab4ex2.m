% Exercise 2: Using the solve function
fprintf('Solve function\n');
syms x y;
S = solve(x^2 + 10*x*y + 4*y^2 == 15, y == 2*x + 1)
S.x
S.y
clear all