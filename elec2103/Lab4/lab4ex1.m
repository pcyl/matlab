%Exercise 1: Using different mathematical functions
% Part 1
fprintf('Part 1: Differentiation\n')
syms t a b c;
diff(exp(-a*t)*sin(b*t + c), 't')
clear all;

% Part 2
fprintf('Part 2: Limits\n')
syms x;
limit(5/(x-2),x,2)
limit(5/(x-2),x,0)
clear all;

% Part 3
fprintf('Part 3: Integration\n')
syms x;
int(1/(1+tan(x)),x,pi/2,0)
clear all;

% Part 4
fprintf('Part 4: Summation\n')
syms k
double(symsum(1/((-1)^(k-1) * k^2),1,20))
clear all;