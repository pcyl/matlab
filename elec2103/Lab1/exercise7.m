t = linspace(0,5.0,101);
x = exp(-0.8*t);
y = cos(8*t);
z = 10.0.*x.*y;
subplot(3,1,1),plot(t,x);
subplot(3,1,2),plot(t,y);
subplot(3,1,3),plot(t,z);