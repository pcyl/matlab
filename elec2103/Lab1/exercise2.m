E = 100 + 10j;
I = 5 + 5j;
Z = 2*exp(j*(degtorad(40)));
V_s = E + Z*I; %formula
[phase,mag] = cart2pol(real(V_s),imag(V_s)); %calcuate the phase and mag
phase = radtodeg(phase); %convert phase from radians to degrees