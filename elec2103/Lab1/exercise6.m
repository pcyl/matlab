t = linspace(0,5.0,101);
x = exp(-0.8*t);
y = cos(8*t);
z = 10.0.*x.*y;
plot(t,z);
title('An exponentially decaying sinusoid');
xlabel('Time'); ylabel('Values of z');
grid()
ylim([-12 12])
