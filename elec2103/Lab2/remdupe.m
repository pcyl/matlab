function res = remdupe(x)
res = [x(1)];
tmp = x(1);

for n = 2:length(x)
    if x(n) ~= tmp
        res(end+1) = x(n);
    end
    tmp = x(n);
end