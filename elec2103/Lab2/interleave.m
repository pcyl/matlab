function res = interleave(x,y)
longervect = x;
shortervect = y;
res = [];
if length(x) < length(y)
    longervect = y;
    shortervect = x;
end

countx = 1;
county = 1;
for n = 1:(2*length(shortervect))
    if mod(n,2) == 0
        res(n) = y(county);
        county = county + 1;
    else
        res(n) = x(countx);
        countx = countx + 1;
    end
end

for n = (length(shortervect)+1):(length(longervect))
    res(end+1) = longervect(n);
end