function x = ex3(y)
[nr,nc] = size(y);
disp('nr = ')
disp(nr)
disp('nc = ')
disp(nc)

if min(nr, nc) ~= 1
    error('Input must be a vector');
end

n = max(nr, nc);
disp('n = ')
disp(n)
z = y(:)';
disp('z = ')
disp(z)
a(2:n) = z(1:n-1);
disp('a = ')
disp(a)
b = a ~= z;
disp('b = ')
disp(b)
b(1) = 1;
disp(b)
x = y(b);