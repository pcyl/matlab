% These lines are comment lines which
% will be displayed when the help function is used % Joe Bloggs 16 Jan 2002
%
x = linspace(0, 5*pi, 100);
y = sin(x);
plot(x, y) % Comments can also be put here