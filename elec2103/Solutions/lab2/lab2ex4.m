function c = lab2ex4(a,b)

n = size(a,2);

if size(a,2) > size(b,2)
    n = size(b,2);
end


c = [];

for i = 1:n
    c = [c, a(i), b(i)];
end

if size(a,2) > size(b,2)
    for i = n+1:size(a,2)
        c = [c, a(i)];
    end
elseif size(b,2) > size (a,2)
    for i = n+1:size(b,2)
        c = [c, b(i)];
    end
end
