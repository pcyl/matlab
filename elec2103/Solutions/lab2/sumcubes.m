function res = sumcubes(x, y)
% SUMCUBES calculates the sum of cubes
% RES = SUMCUBES(X, Y) returns the sum of the cubes of X % and Y, element by element.
% HY 17/1/02
%
res = x.^3 + y.^3;