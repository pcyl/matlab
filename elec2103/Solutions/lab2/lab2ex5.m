function x = lab2ex5(y)
% Exercise 3 Laboratory 2 
[nr nc] = size(y);
if min(nr, nc) ~= 1
error('Input must be a vector'); 
end
n = max(nr, nc);
z = y(:)'; % a single column vector with all elements of y, 
           % then transpose it to a single row vector
a(2:n) = z(1:n-1); % a is [0 1 2 2 2 3 0 1 0 0]
                   % z is [1 2 2 2 3 0 1 0 0 4] 
b = a ~= z; % b is a boolean vector [1 1 0 0 1 1 1 1 0 1]
b(1) = 1;  % make sure the first element of y can be displayed
x = y(b); % x is [1 2 3 0 1 0 4]
