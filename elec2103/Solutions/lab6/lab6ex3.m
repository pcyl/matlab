function coldata =lab6ex3 (filename,colnumber)
fp=fopen(filename,'r');
numcols=fread(fp,1,'int16');
numrows=fread(fp,1,'int16');
numbytes=4;
offset=4;
seekpos=offset +(colnumber-1)*numrows*numbytes;
fseek(fp,seekpos,'bof');
coldata=fread(fp,numrows,'float32');
fclose(fp);