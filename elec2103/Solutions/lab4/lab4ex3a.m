syms V w
L=20*10^-3;
C=10^-6;
R=1000;
%R=100
i=10;

ZL = 1j*w*L;
ZC = 1/(1j*w*C);
Z = (R^-1+ZC^-1+ZL^-1)^-1;
V = Z*i;
Vm = abs(V);
ezplot(Vm,[100 20000])
w0 = 1/sqrt(L*C)
Vmmax = subs(Vm,w,w0)



