syms s R L C V0 a w ze;
L=1;C=1;V0=1;
b=R/(2*L)-a;
w=1;
eq= s^2+2*w*ze*s+1;
disp('============part A1=======')
b=subs(b,'a',ze*w);
solve(b,R)
disp('=============part A2==========')
ze=1;
eq=subs(eq,'ze',ze);
S=solve(eq,s)
disp('=============part B==========')
syms k1 k2 t
Vc= k1*exp(S(1)*t)+k2*exp(S(2)*t)
Vcom=V0+Vc
Vcom1=subs(Vcom,'t',0)
dVcom=diff(Vcom,t)
dVcom1=subs(dVcom,'t',0)
[k1,k2]=solve(Vcom1,dVcom1)
double(k1)
double(k2)