
% p=[0.01:1.5];
% q=[0:15];
syms z a R s V0 C L
w0=1/sqrt(L*C);
z= 1;
a=z*w0;
R=2*L*a;
R=subs(R,[C L],[1 1])
s=solve(L*C*s^2+R*C*s+1);
s=subs(s,[C L],[1 1])
syms k1 k2 t
V0=1;
k1 = s(2)*V0/(s(1)-s(2));
k2 = s(1)*V0/(s(1)-s(2));
vc=V0+k1*exp(s(1)*t)+k2*exp(s(2)*t);
vc=subs(vc,t,10)

% k=length(p);
% for n=1:k
%      for m=1:k
%          Vc(m,n)=subs(vc,[t z] ,[p(n) q(m)] );
%      end
% end
% Vc=real(vc);
% [t, z]=meshgrid(p,q);
% mesh(t,z,Vc)


