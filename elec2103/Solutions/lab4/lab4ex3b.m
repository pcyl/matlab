syms V w
L=20*10^-3;
C=10^-6;
R=1000;
i=10;


ZL = 1j*w*L;
ZC = 1/(1j*w*C);
Z = (R^-1+ZC^-1+ZL^-1)^-1;
V = Z*i;

wr=100:20000;
Vv=subs(V,w,wr);
Va=angle(Vv)/pi*180;
plot(wr,Va)