function [ Zt ] = zladder( Z, Y )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
a=length(Z);
if a < 2
    Zt=Z(1)+Y(1)^-1;
end
for n=a:-1:2
    Zt=Z(n)+Y(n)^-1;
    Y(n-1)=Zt^-1+Y(n-1);
end    


end

