syms e x v

S = dsolve('200*x-20*v-Dv=0','100*x+De=0','2*e-2*v-Dx=0','e(0)=5','x(0)=0','v(0)=0');
E = S.e;
E = vpa(E,4);
pretty(E)
ezplot(E,[0 1]);
ylim([-5 8])