syms R1 R2 R3 C1 C2 E1 E2 V s

A=V/(R2+1/(s*C1))+E2/R3;
B=(E1-V)/R1-V/(R2+1/(s*C1))-(V-E2)*s*C2;
S=solve(A,B,V,E2);

pretty(collect(S.E2/E1))
