syms E1 E2 I1 I2 s t 

A1=E1-3/2*s*(I1+I2)-I1/(4/3*s);
A2=E2-I2;
A3=I1/(4/3*s)-1/2*s*I2-E2;
S=solve(A1,A2,A3,E2,E1,I2);
E1=S.E1;
E2=S.E2;
G=E2/E1;
G=subs(G,s,w*j);
G=abs(G);
w=linspace(-2*pi,2*pi);
plot(w,G)