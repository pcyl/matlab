function Pi = lab5ex6 (n)

rand('state',100*sum(clock));
A=rand(n,2);
a=0;
b=size(A,1);
for k=1:n
    d=abs(A(k,1)+A(k,2)*1j);
    if d<=1
        a=a+1;
    end
end
Pi=4*a/b;