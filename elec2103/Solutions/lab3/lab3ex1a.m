
% T4.5-3 Write a program to produce the following matrix:
% A=[4 8 12; 10 14 18; 16 20 24; 22 26 30]
A=ones(4,3);
for r=1:4
    for c=1:3
        A(r,c)=6*r-2+4*(c-1);
    end
end
A