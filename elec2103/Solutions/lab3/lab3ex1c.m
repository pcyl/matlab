% T4.7?1 Write a program using the switch structure to input one angle,
% whose value may be 45,-45,135,or -135�,and display the
% quadrant(1,2,3,or4) containing the angle.

angle = input ('Enter an angle: ');
switch angle
    case 45
        disp('1')
    case -45
        disp('4')
    case 135
        disp('2')
    case -135
        disp('3')
    otherwise
        disp('Input Unknown')
end