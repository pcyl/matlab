% Chapter 4, Problem 26
% Electrical resistors are said to be connected ?in series? 
% if the same current passes through each and ?in parallel? 
% if the same voltage is applied across each. If in series, 
% they are equivalent to a single resistor whose resistance is given by
% R = R1 + R2 + R3 + R4 + Rn
% If in parallel, their equivalent resistance is given by
% 1/R = 1/R1+ 1/R2 + 1/R3 + 1/R4 + 1/Rn
% Write an M-file that prompts the user for the type of connection (series or parallel) 
% and the number of resistors n and then computes the equivalent resistance.


R= input ('Enter value of resistor: ');
n= size(R,2);
type = input ('Enter an type (series or parallel): ','s');
switch type
    case 'series'
        Rt = sum(R);
        disp (Rt)
    case 'parallel'
        Rt = sum(ones(1,n)./R(1:n));
        Rt = 1.0/Rt1;
        disp (Rt)
    otherwise
        disp('Input Unknown')
end