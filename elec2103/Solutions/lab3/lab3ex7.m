% Chapter 4, Problem 37 
% 

% a = [5 0 0 150 0;0 100 0 -150 250;0 0 200 0 -250;1 -1 0 -1 0;0 1 -1 0 -1];
% 
% 
% for v2 = 0:100
%     b = [100;0;-v2;0;0];
%     i = a\b;
%     if (abs(i(1,1))<=1 && abs(i(2,1))<=1 && abs(i(3,1))<=1 ...
%             && abs(i(4,1))<=1 && abs(i(5,1))<=1)
%         
%         v2
%         
%        
%     end
% end
% 
% R3 = 150:250;
% plot(R3,v2);
% grid

R3 = 150:250;
for m=1:length(R3)
    m
    coefmat = [5000 0 0 150000 0; 0 100000 0 -150000 250000; 0 0 R3(m) 0 -250000; 1 -1 0 -1 0; 0 1 -1 0 -1];
    index=0;
    V2 =[0.1:0.1:400]
    for k=1:length(V2)
        index=index+1;
        constmat = [100 0 -V2(k) 0 0].';
        current(:,index) = 1000*coefmat\constmat;
        if sum(abs(current(:,index))>1)>0
            allowed_voltage(m,index)=0;
        else
            allowed_voltage(m,index)=1;
        end
    end
    tmp =find (allowed_voltage(m,:)==1);
    V2_min(1,m)=V2(min(tmp));
    V2_max(1,m)=V2(max(tmp));
end

plot(V2_max(1,:),V2_min(1,:))
grid
