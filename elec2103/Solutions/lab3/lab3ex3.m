function gwam = lab3ex3(a)

mark = a(:,1);
cpv = a(:,2);
year = a(:,3);
sumtopc = mark.*cpv.*year;
sumtop = sum(sumtopc);
sumbottomc = cpv.*year;
sumbottom = sum(sumbottomc);
gwam = sumtop/sumbottom;

if gwam>=75
    disp('H1');
end
if gwam>=70 && gwam<75;
    disp('H2(1)')
end
if gwam>=65 && gwam<70;
    disp('H2(2)')
end
