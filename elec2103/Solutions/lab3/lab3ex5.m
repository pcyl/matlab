function ei = lab3ex5(k)
a= 1;
b= sqrt(1-k);

while a-b > eps
    x = a;
    y = b;
    a= (x+y)/2;
    b= sqrt(x.*y);
end

g=a;

ei=(g.^(-1)).*(pi/2);
