function p = lab3ex4(n)

pa=[1 0];
pb=[1];
if(n==0)
    p=[1];
elseif(n==1)
    p=[1 0];
else
    for i=2:n
        pl=((2*i-1)/i)*[pa 0];
        pr=((i-1)/i)*[0 0 pb];
        p= pl - pr;
        pb=pa;
        pa=p;
    end
end



 