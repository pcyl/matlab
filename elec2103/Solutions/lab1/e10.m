t = [0:0.05: 2*pi];
x = 3 + 6 * cos(t);
y = -2 + 9 * sin(t);
plot(x,y);
grid

axis('equal')