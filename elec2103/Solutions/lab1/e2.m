s = 2 * pi /9;

[x, y] = pol2cart(s , 2);

Z = x + j*y;

E = 100 + j*10;

IL = 5 +j*5;

Vs = E + (Z * IL);

mag = abs(Vs)

rad = angle(Vs);

phase = rad /pi * 180