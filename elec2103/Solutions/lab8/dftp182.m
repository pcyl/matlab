function YyYypr = dftp182(TtTt,YyYy)

 x = YyYy;

l = length(YyYy);
dh = get(gcf,'user');
lambda= dh(13);
YyYypr =  lambda.*x;
if (length(YyYypr) < l) YyYypr = YyYypr*ones(1,l);end
