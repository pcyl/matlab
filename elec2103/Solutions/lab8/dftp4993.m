function YyYypr = dftp4993(TtTt,YyYy)

 x = YyYy;

l = length(YyYy);
dh = get(gcf,'user');
lambda= dh(13);
YyYypr = x.^2;
if (length(YyYypr) < l) YyYypr = YyYypr*ones(1,l);end
