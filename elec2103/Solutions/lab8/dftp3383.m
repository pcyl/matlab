function YyYypr = dftp3383(TtTt,YyYy)

 x = YyYy;

l = length(YyYy);
dh = get(gcf,'user');
lambda= dh(13);
YyYypr = x.^3-3.*x.^2-10.*x;
if (length(YyYypr) < l) YyYypr = YyYypr*ones(1,l);end
