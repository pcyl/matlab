M = [-2 2;0 -1];
[Va,Da] = eig(M)
%sink

M = [1 -8;2 1];
[Vb,Db] = eig(M)
%source

M = [9 -11;-11 9];
[Vc,Dc] = eig(M)
%saddle

M = [8 0;-5 3];
[Vd,Dd] = eig(M)
%saddle

M = [-2 -2;4 1];
[Ve,De] = eig(M)
%sink

M = [2 -2;4 -2];
[Vf,Df] = eig(M)
%center