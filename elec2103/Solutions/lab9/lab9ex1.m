%Use tf to create a TF object SYS to model this system
G1=tf([5 30 25],[1 18.2 115.6 342.4 64]);
%Apply tf to SYS to find the zpk description
[Z,P,K]=zpkdata(G1)
G2=zpk(Z,P,K)
%Apply pole and zero functions
pole(G1)
zero(G1)
%Apply ssdata to SYS to find a set of trices A, B, C and D
[A,B,C,D]=ssdata(G1)
%Apply tfdata to check
[num,den]=tfdata(G2,'v')