G=tf(500,conv([1 10],[1 1 26]));
bode(G)

% Magnitude at very low frequencies: 5.7 dB

% Peak value: 18.9 dB

% Frequency at bandwidth: 7.4 rad/sec (at 2.7dB)