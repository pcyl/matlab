G = tf(260,conv([1 5],[1 2 26]))

t1 = [0:0.02:1];
t2 = [1.02:0.02:2];
t3 = [2.02:0.02:3];
t4 = [3.02:0.02:6];
t= [t1 t2 t3 t4];

u1 = t1;
u2 = t2./t2;
u3 = -t2./t2;
u4 = t4.*0;
u = [u1 u2 u3 u4];

lsim(G,u,t)

