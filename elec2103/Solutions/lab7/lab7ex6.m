
syms x y
v = [x;y];
f1 = x^2 + y^2 - 4;
f2 = y + 2.1/x - 0.3;
f = [f1;f2];
X = [3;-1.5];
a = 10^-6;
results = lab7ex5f(f,v,X,a);
celldisp(results(1))

syms z
v = [x;y;z];
f1 = sin(x) + y^2 + log(z) - 7;
f2 = 3*x + 2^y -z^3 + 1;
f3 = x + y + z - 5;
f = [f1;f2;f3];
X = [0;2;2];
a = 10^-6;
results = lab7ex5f(f,v,X,a);
celldisp(results(1))