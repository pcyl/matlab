x=[-4*pi:0.01:4*pi].'; y=cos(2*x).*cos(10*x+pi/5);

M=[cos(8*x),sin(8*x),cos(10*x),sin(10*x),cos(12*x),sin(12*x)]
[Q0,R0]=qr(M,0);
z0=Q0.'*y;
C=R0\z0