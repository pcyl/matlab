syms z
f=z^3-1;
a=10^-6;
A=lab7ex5f(f,z,0.5,a);
celldisp(A(1))
B=lab7ex5f(f,z,0.5j,a);
celldisp(B(1))
C=lab7ex5f(f,z,-0.5j,a);
celldisp(C(1))