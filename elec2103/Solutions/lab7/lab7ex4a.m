x = [-10:0.01:10];
y = (x-5).^(-1);
plot(x,y)

fzero(inline('(x-5).^(-1)'),[4.8 5.2])

%%The graph is a hyperbola. When x=5, the sign changes,but x=5 is not the
%%root of the function.
%%Solution: Check the function graph firstly.Eliminate the wrong result.