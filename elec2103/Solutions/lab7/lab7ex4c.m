x=[-10:0.01:10];
y1=sin(1./x);
y2=x-tanh(3*x);
y3=cos((x.^2+5)./(x.^4+1));
plot(x,y1)
figure
plot(x,y2)
figure
plot(x,y3)
a= fzero(inline('sin(1./x)'),0.18)
b1=fzero(inline('x-tanh(3*x)'),[-2 -0.5])
b2=fzero(inline('x-tanh(3*x)'),[-0.5 0.5])
b3=fzero(inline('x-tanh(3*x)'),[0.5 2])
c1=fzero(inline('cos((x.^2+5)./(x.^4+1))'),[-2 -1])
c2=fzero(inline('cos((x.^2+5)./(x.^4+1))'),[-1 0])
c3=fzero(inline('cos((x.^2+5)./(x.^4+1))'),[0 1])
c4=fzero(inline('cos((x.^2+5)./(x.^4+1))'),[1 2])