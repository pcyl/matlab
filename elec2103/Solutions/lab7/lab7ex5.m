syms x1 x2 x3
f1 = -0.7 + 8.4 * sin(x1) + 7.35 * x3 * sin(x1-x2);
f2 = 3 + 10*x3*sin(x2)+7.35*x3*sin(x2-x1);
f3 = 1.25 - 10*x3*cos(x2)-7.35*x3*cos(x2-x1) + 17*x3^2;
f = [f1;f2;f3];
v = [x1;x2;x3];
a = 10^-6;
x = [0;0;1];
results=lab7ex5f(f,v,x,a);


celldisp(results(1))