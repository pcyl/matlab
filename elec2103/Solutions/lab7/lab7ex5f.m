function results = lab7ex5f(f,v,x,a)
J = jacobian(f);
xa= x;
boolean = true;
n = 0;
while boolean == true
    xb = xa;
    Ja = J;
    fa = f;
    for k = 1:length(v)
        Ja = subs(Ja, v(k), xb(k));
        fa = subs(fa, v(k), xb(k));
    end
    xa = xb - inv(Ja)*fa;
    boolean = false;
    for t = 1:length(xa)
        if norm(fa(t)) > a
            boolean = true;
        end
    end
    n = n+1;
end
results(1)={xa};
results(2)={fa};
results(3)={n};
results(4)={Ja};
