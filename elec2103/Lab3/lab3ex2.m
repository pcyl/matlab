x = linspace(2*pi,-2*pi,100);
y = x;
[xi,yi] = meshgrid(x,y);
z = sin(xi).*cos(yi);
mesh(xi,yi,z);