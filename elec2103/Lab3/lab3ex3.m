Res = [66 6 1; 54 6 1; 
    68 4 2; 77 4 2; 70 4 2; 
    76 4 3; 70 4 3; 69 4 3;
    74 12 4];

GWAM = (sum(Res(1:end,1).*Res(1:end,2).*Res(1:end,3)))/sum((Res(1:end,2).*Res(1:end,3)));
fprintf('\nGWAM = %f\n', GWAM);

if (GWAM >= 75.0)
    fprintf('First Class\n\n');
elseif (GWAM >= 70.0) 
    fprintf('Second Class, Division 1\n\n');
elseif (GWAM >= 65.0)
    fprintf('Second Class, Division 2\n\n');
else
    fprint('No Honours\n\n');
end