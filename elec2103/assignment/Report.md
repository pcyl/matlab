Report
======

Topic
-----
National life expectancy of men and women.

### Aim
Analysing the life expectancy of Australians to provide a model to be as accurate as possible in predicting future life expectancies and the differences between the ages of men and women.

### Analysis
There are two parts to the analysis of the data. Each consecutive part builds on the evaluation of the prior
part.

#### Part 1
The data was plotted from the earliest date available, 1890 to 2012 with a predicted linear fit
with the residuals included in the plot. Based on the analysis of the residuals the scatter points was non-random
for both men and women, indicating that the model was a bad fit despite having a very good r-squared value. This
indicated that the fit equation is bad for predicting future life expectancies. To further confirm my analysis, 
the Monte Carlo method was used to see the goodness of fit which resulted in a better understanding of the problem.
The Monte Carlo method indicated that the male fit was under-estimated and the female fit was over-estimated.
This meant that I had to choose a narrower set of points to get a better fit that would result in a more accurate model.

#### Part 2
The dates from 1995 to 2012 were chosen as points to provide a more accurate model of the predicting the life expectancies
of men and women. When the data was plotted with a linear fit model it provided a very good r-squared value, however there was still a non-randomness of scatter points in the residuals indicating a bad fit. The new residuals was an improvement over the previous model. To further analyse the goodness of the model, the Monte Carlo method was used again for both men and women. The results showed that the model is accurate in predicting the life expectancies with the linear fit perfectly settled in the center of the random fits.

### Summary
On the evaluation of the analysis above, the differences in the trends between the life expectancies of men and women in Australia remains mostly the same with the prediction model supporting this.

References
----------
* ABS Life Exptectancy Data
[http://www.abs.gov.au/AUSSTATS/abs@.nsf/Lookup/3105.0.65.001Main+Features12014?OpenDocument](http://www.abs.gov.au/AUSSTATS/abs@.nsf/Lookup/3105.0.65.001Main+Features12014?OpenDocument)
* Introduction to Monte Carlo Simulation
[http://physics.gac.edu/~huber/envision/instruct/montecar.htm](http://physics.gac.edu/~huber/envision/instruct/montecar.htm)
* Regression Analysis: How Do I Interpret R-squared and Assess the Goodness-of-Fit?
[http://blog.minitab.com/blog/adventures-in-statistics/regression-analysis-how-do-i-interpret-r-squared-and-assess-the-goodness-of-fit](http://blog.minitab.com/blog/adventures-in-statistics/regression-analysis-how-do-i-interpret-r-squared-and-assess-the-goodness-of-fit)