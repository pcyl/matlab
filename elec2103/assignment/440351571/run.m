%% PLAGIARISM AGREEMENT
text1='I certify that:';
text2='(1) I have read and understood the University of Sydney Academic Dishonesty and Plagiarism Policy  (found by following the  Academic Honesty link on the Unit web page).';
text3='(2) I understand that failure to comply with the Student Plagiarism: Coursework Policy and Procedure can lead to the University commencing proceedings against me for potential student misconduct under Chapter 8 of the University of Sydney By-Law 1999 (as amended).';
text4='(3) This Work is substantially my own, and to the extent that any part of this Work is not my own I have indicated that it is not my own by Acknowledging the Source of that part or those parts of the Work as described in the assignment instructions.';
text5='Name: Patrick Chun Ying Lai'; % Please put your name in this string
text6='Date: 13 October 2015'; % Please put the date in this string
text7='Click Run, and the Report should appear along with the plots. Make sure to click OK to the prompts as they appear. Generated plots are available in this folder if needed.';% Replace this string
waitfor(msgbox(char(text1,' ',text2,' ',text3,' ',text4,' ',text5,' ',text6,' ',text7),'ELEC2103/9103 Assignment 2015'));

load data.mat
disp('Opening Report');
web('Report.html')
%% Constants
n = 100; %number of evaluations for Monte Carlo

%% PART 1 ======================================================================
waitfor(msgbox(char('Part 1: 1890 to 2012')));
disp('Running Part 1');
%% Plot data from 1890 to 2012
figure('Name','Life expectancy from 1890 to 2012','numbertitle','off')
subplot(2,1,1); plot(Year, Age_Male,'-bo',Year,Age_Female,'-ro')
grid on
hold on
legend('Male','Female', 'Location', 'Northwest');
%% Fitting the Data

f = 'poly1';
fit_male = fit(Year,Age_Male,f);
plot(fit_male,'g-.');
fit_female = fit(Year,Age_Female,f);
plot(fit_female,'k-.');
%% Plot Labelling
legend('Male','Female', 'Male Fit', 'Female Fit', 'Location', 'Northwest');
xlabel('Year'); ylabel('Age'); title('National Life Expectancy');
hold off
%% Plot Residuals - Indicates a very bad fit due to the non-randomness of points
subplot(2,1,2);
plot(fit_male,Year,Age_Male,'ko','residuals')
hold on
plot(fit_female,Year,Age_Female,'bx','residuals')
title('Residuals');
hold off
%% Monte Carlo (MALE) Indicates a bad fit with underestimation

figure('Name','Life expectancy from 1890 to 2012 (Monte Carlo)','numbertitle','off')
for i=1:n
x1 = ( randn(43,1) * std(Age_Male) ) + Age_Male;
randfit = fit(Year,x1,'poly1');
mrandfit1 = plot(randfit);
hold on
legend hide
end
%% Monte Carlo (FEMALE) Indicates a bad fit with overestimation

for i=1:n
x1 = ( randn(43,1) * std(Age_Female) ) + Age_Female;
randfit = fit(Year,x1,'poly1');
frandfit1 = plot(randfit,'b');
legend hide
end

malefit1 = plot(fit_male,'k-');
set(malefit1,'linewidth',2)
femalefit1 = plot(fit_female,'g-');
set(femalefit1,'linewidth',2);
legend([mrandfit1,malefit1,frandfit1,femalefit1],'Male random fits','Male Fit','Female random fits','Female Fit', 'Location','Northwest');
hold off;
%% =============================================================================


%% PART 2 ======================================================================
waitfor(msgbox(char('Part 2: 1995 to 2012')));
disp('Running Part 2');
%% Plot with data from 1995 to 2012 (IMPROVED FIT)
figure('Name','Life expectancy from 1995 to 2012','numbertitle','off')
subplot(2,1,1); plot(Year(30:end), Age_Male(30:end),'-bo',Year(30:end),Age_Female(30:end),'-ro')
grid on
hold on
legend('Male','Female', 'Location', 'Northwest');
%% Fitting the Data
f = 'poly1';
fit_male = fit(Year(30:end),Age_Male(30:end),f);
plot(fit_male,'g-.');
fit_female = fit(Year(30:end),Age_Female(30:end),f);
plot(fit_female,'k-.');
%% Plot Labelling
legend('Male','Female', 'Male Fit', 'Female Fit', 'Location', 'Northwest');
xlabel('Year'); ylabel('Age'); title('National Life Expectancy');
xlim('auto');
hold off
%% Plot Residuals - Indicates a bad fit still due to non-randomness of points
subplot(2,1,2);
plot(fit_male,Year(30:end),Age_Male(30:end),'ko','residuals')
hold on
plot(fit_female,Year(30:end),Age_Female(30:end),'bx','residuals')
title('Residuals');
hold off
%% Monte Carlo (MALE) Indicates a very good fit

figure('Name','Life expectancy from 1995 to 2012 (Monte Carlo)','numbertitle','off')
for i=1:n
x1 = ( randn(14,1) * std(Age_Male(30:end)) ) + Age_Male(30:end);
randfit = fit(Year(30:end),x1,'poly1');
mrandplot2 = plot(randfit);
hold on
legend hide
end
%% Monte Carlo (FEMALE) Indicates a very good fit

for i=1:n
x1 = ( randn(14,1) * std(Age_Female(30:end)) ) + Age_Female(30:end);
randfit = fit(Year(30:end),x1,'poly1');
frandplot2 = plot(randfit,'b');
hold on
legend hide
end

malefit2 = plot(fit_male,'k-');
set(malefit2,'linewidth',2);
femalefit2 = plot(fit_female,'g-');
set(femalefit2,'linewidth',2);
legend([mrandplot2,malefit2,frandplot2,femalefit2],'Male random fits','Male Fit','Female random fits','Female Fit','Location','Northwest');
hold off;

disp('Finished');
%% =============================================================================
