function [ CapTot ] = hw9q2( C0,C1 )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
a = C0 + 16;
b1 = inv((1/4) + (1/C1)) + 1.6;
b2 = inv(1/b1 + 1/12);
c = b2 + 5;
CapTot = inv(1/a + 1/c);
end

