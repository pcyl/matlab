function [ v ] = hw5q1( i1, i2 )
%Solves Q1 and Q2 HW5 of elec1103
a = [6 -5; 6 -7];
b = [i1*40; i2*48];

v = a\b;
end

