function [ time ] = hw10q1( Vo, Io,Co )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Vc_0 = (9/(9 + 3)) * Vo;
Vc_inf = -(Io * 10^-3) * (40 * 10^3);
V_Th = Vc_inf;
R_Th = 10 + 40;
t = R_Th * (Co*10^-6);
time = -t*log((-1*Vc_inf)/(Vc_0 - Vc_inf));
end

