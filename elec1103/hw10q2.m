function [ amps ] = hw10q2( Ro, Lo, time )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
vo = 2000/(6 + 40/Ro);
i_0 = vo/Ro;
V_Th = 200;
R_Th = Ro + 30;
t = (Lo * 10^-3)/R_Th;
i_inf = V_Th/R_Th;

amps = i_inf + (i_0 - i_inf) * exp((-time)/t);

end

