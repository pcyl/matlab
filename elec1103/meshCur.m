function [ P ] = meshCur( R )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
Rtot = 75+R;
a = [40,-10,-25; -10,40,-30; -25,-50, Rtot];
b = [660;0;0];
temp = a\b;
P = (temp(2)-temp(3)) * 20 * temp(2);
end