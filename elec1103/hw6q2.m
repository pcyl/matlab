function [ power ] = hw6q2( V )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
a = [25 -3 -20 0; -3 12 -4 0; -20 -4 25 10; 1 -1 0 1];
b = [V;0;0;0];
i = linsolve(a,b);
power = (i(3) - i(1))^2 * 20;

end

